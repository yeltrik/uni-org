<?php

use Illuminate\Support\Str;

return

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    [
        // Connection
        'uni_org' => [
            'driver' => 'sqlite',
            'url' => env('DATABASE_URL'),
            'database' => env('UNI_ORG_DB_DATABASE', database_path('uni_org.sqlite')),
            'prefix' => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],

        // SQLITE
        'uni_org_sqlite_example' => [
            'driver' => 'sqlite',
            'url' => env('DATABASE_URL'),
            'database' => env('UNI_ORG_DB_DATABASE', database_path('uni_org.sqlite')),
            'prefix' => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],

        // MYSQL
        'uni_org_mysql_example' => [
            'driver' => 'mysql',
            'url' => env('DATABASE_URL'),
            'host' => env('UNI_ORG_DB_HOST', '127.0.0.1'),
            'port' => env('UNI_ORG_DB_PORT', '3306'),
            'database' => env('UNI_ORG_DB_DATABASE', 'uni_org'),
            'username' => env('UNI_ORG_DB_USERNAME', 'uni_org'),
            'password' => env('UNI_ORG_DB_PASSWORD', ''),
            'unix_socket' => env('UNI_ORG_DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ],

    ];
