<?php

namespace Yeltrik\UniOrg\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;
use Yeltrik\UniOrg\database\factories\CollegeFactory;

/**
 * Class College
 *
 * @property int id
 * @property int university_id
 * @property string name
 * @property string abbr
 *
 * @property University university
 * @property Collection departments
 *
 * @package Yeltrik\UniOrg\app\models
 */
class College extends Model
{
    use HasFactory;

    protected $connection = 'uni_org';
    public $table = 'colleges';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function departments()
    {
        return $this->hasMany(Department::class);
    }

    /**
     * @return CollegeFactory
     */
    public static function newFactory()
    {
        return new CollegeFactory();
    }

    /**
     * @return BelongsTo
     */
    public function university()
    {
        return $this->belongsTo(University::class);
    }

}
