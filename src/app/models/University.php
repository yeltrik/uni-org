<?php

namespace Yeltrik\UniOrg\app\models;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Yeltrik\UniOrg\database\factories\UniversityFactory;

/**
 * Class University
 *
 * @property int id
 * @property string name
 * @property string abbr
 *
 * @property Collection campuses
 * @property Collection colleges
 *
 * @package Yeltrik\UniOrg\app\models
 */
class University extends Model
{
    use HasFactory;

    protected $connection = 'uni_org';
    public $table = 'universities';

    /**
     * Create a new factory instance for the model.
     *
     * @return Factory
     */
    protected static function newFactory()
    {
        return new UniversityFactory();
    }

    /**
     * @return HasMany
     */
    public function campuses()
    {
        return $this->hasMany(Campus::class);
    }

    /**
     * @return HasMany
     */
    public function colleges()
    {
        return $this->hasMany(College::class);
    }

}
