<?php

namespace Yeltrik\UniOrg\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Yeltrik\UniOrg\database\factories\CampusFactory;

/**
 * Class Campus
 *
 * @property int id
 * @property int university_id
 * @property string name
 * @property string abbr
 *
 * @property University university
 *
 * @package Yeltrik\UniOrg\app\models
 */
class Campus extends Model
{
    use HasFactory;

    protected $connection = 'uni_org';
    public $table = 'campuses';

    /**
     * @return CampusFactory
     */
    protected static function newFactory()
    {
        return new CampusFactory();
    }

    /**
     * @return BelongsTo
     */
    public function university()
    {
        return $this->belongsTo(University::class);
    }

}
