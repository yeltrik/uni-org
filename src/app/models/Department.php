<?php

namespace Yeltrik\UniOrg\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Yeltrik\UniOrg\database\factories\DepartmentFactory;

/**
 * Class Department
 *
 * @property int id
 * @property int university_id
 * @property int college_id
 * @property string name
 * @property string abbr
 *
 * @property University university
 * @property College college
 *
 * @package Yeltrik\UniOrg\app\models
 */
class Department extends Model
{
    use HasFactory;

    protected $connection = 'uni_org';
    public $table = 'departments';

    /**
     * @return BelongsTo
     */
    public function college()
    {
        return $this->belongsTo(College::class);
    }

    /**
     * @return DepartmentFactory
     */
    public static function newFactory()
    {
        return new DepartmentFactory();
    }

    /**
     * @return BelongsTo
     */
    public function university()
    {
        return $this->belongsTo(University::class);
    }
}
