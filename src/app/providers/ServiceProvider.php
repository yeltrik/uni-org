<?php

namespace Yeltrik\UniOrg\app\providers;


use Illuminate\Support\Facades\Gate;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    const ADMIN_EMAILS_ENV_KEY = "ADMIN_EMAILS";

    CONST CONFIG_DATABASE_CONNECTION_FILE_NAME = 'yeltrik-uni-org-database-connections.php';

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        if (file_exists(config_path(static::CONFIG_DATABASE_CONNECTION_FILE_NAME))) {
            // User Customizable
            $this->mergeConfigFrom(
                config_path(static::CONFIG_DATABASE_CONNECTION_FILE_NAME), 'database.connections'
            );
            //dd(config('database'));
        } elseif (file_exists(static::packageSrc() . 'config/' . static::CONFIG_DATABASE_CONNECTION_FILE_NAME)) {
            // Non Customizable
            $this->mergeConfigFrom(
                static::packageSrc() . 'config/' . static::CONFIG_DATABASE_CONNECTION_FILE_NAME, 'database.connections'
            );
            //dd(config('database'));
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(static::packageSrc() . 'database/migrations');
        $this->loadRoutesFrom(static::packageSrc() . 'routes/web.php');
        $this->loadViewsFrom(static::packageSrc() . 'resources/views', 'uniOrg');

        if ($this->app->runningInConsole()) {
            $this->publishResources();
        }

        Gate::define('isAdmin', function ($user) {
            $emails = explode(",", env(static::ADMIN_EMAILS_ENV_KEY));
            if (empty($emails)) {
                dd('Need to Set ' . static::ADMIN_EMAILS_ENV_KEY . ' Addresses in .env');
            } else {
                $email = auth()->user()->email;
                return in_array(strtolower($email), $emails);
            }
            return FALSE;
        });

        Gate::define('viewUniversities', function ($user) {
            return TRUE;
        });
    }

    protected static function packageSrc()
    {
        return __DIR__ . '/../../';
    }

    protected function publishResources()
    {
        // User Customizable
        $this->publishes([
            static::packageSrc() . 'config/' . static::CONFIG_DATABASE_CONNECTION_FILE_NAME => config_path(static::CONFIG_DATABASE_CONNECTION_FILE_NAME),
        ], 'config');

        $this->publishes([], 'uniOrg');
    }

}
