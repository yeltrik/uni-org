<?php

namespace Yeltrik\UniOrg\app\policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;
use Yeltrik\UniMbr\app\models\DepartmentHead;
use Yeltrik\UniMbr\app\models\Member;
use Yeltrik\UniOrg\app\models\Department;

class DepartmentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny(User $user)
    {
        if (Gate::allows('isAdmin')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function view(User $user, Department $department)
    {
        $member = Member::query()
            ->where('email', '=', $user->email)
            ->first();

        return (
            $this->viewAny($user) ||
            (
                $member instanceof Member &&
                $member->departmentHead instanceof DepartmentHead &&
                (int)$department->id === (int)$member->departmentHead->department_id
            )
        );
        // TODO: or if is Dean of College that this department belongs to
    }

}
