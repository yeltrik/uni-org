<?php

namespace Yeltrik\UniOrg\app\policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Yeltrik\UniOrg\app\models\University;

class UniversityPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return TRUE;
    }

    /**
     * @param User $user
     * @param University $university
     * @return bool
     */
    public function view(User $user, University $university)
    {
        return TRUE;
    }
}
