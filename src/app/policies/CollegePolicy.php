<?php

namespace Yeltrik\UniOrg\app\policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;
use Yeltrik\UniOrg\app\models\College;

class CollegePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny(User $user)
    {
        if (Gate::allows('isAdmin')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function view(User $user, College $college)
    {
        return $this->viewAny($user);
    }

}
