<?php

namespace Yeltrik\UniOrg\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Yeltrik\UniMbr\app\models\DepartmentHead;
use Yeltrik\UniMbr\app\models\Faculty;
use Yeltrik\UniMbr\app\models\Staff;
use Yeltrik\UniOrg\app\models\College;
use Yeltrik\UniOrg\app\models\Department;
use Yeltrik\UniOrg\app\models\University;

class DepartmentController extends Controller
{
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param University $university
     * @param College $college
     * @return void
     * @throws AuthorizationException
     */
    public function index(University $university, College $college)
    {
        $this->authorize('viewAny', Department::class);
        $departments = $college->departments;

        return view('uniOrg::university.college.department.index', compact([
            'university', 'college', 'departments'
        ]));
    }

    /**
     * @param University $university
     * @param Department $department
     * @throws AuthorizationException
     */
    public function show(University $university, Department $department)
    {
        $university = $department->university;
        $college = $department->college;

        if ( class_exists(DepartmentHead::class)) {
            $departmentHeads = DepartmentHead::query()->where('department_id', '=', $department->id)->get();
        } else {
            $departmentHeads = new Collection();
        }
        if ( class_exists(Faculty::class)) {
            $faculty = Faculty::query()->where('department_id', '=', $department->id)->get();
        } else {
            $faculty = new Collection();
        }
        if ( class_exists(Staff::class)) {
            $staff = Staff::query()->where('department_id', '=', $department->id)->get();
        } else {
            $staff = new Collection();
        }
        $this->authorize('view', $department, compact([
            'university', 'college', 'department',
            'faculty', 'staff'
        ]));

//        dd([$faculty, $staff]);
        return view('uniOrg::university.college.department.show', compact([
            'university', 'college', 'department',
            'departmentHeads', 'faculty', 'staff'
        ]));
    }

}
