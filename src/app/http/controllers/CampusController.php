<?php

namespace Yeltrik\UniOrg\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Yeltrik\UniOrg\app\models\Campus;
use Yeltrik\UniOrg\app\models\University;

class CampusController extends Controller
{
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @param University $university
     * @throws AuthorizationException
     */
    public function index(University $university)
    {
        $this->authorize('viewAny', Campus::class);
        $campuses = $university->campuses;

        return view('uniOrg::university.campus.index', compact([
            'university', 'campuses'
        ]));
    }

    /**
     * @param University $university
     * @param Campus $campus
     * @throws AuthorizationException
     */
    public function show(University $university, Campus $campus)
    {
        $this->authorize('view', $campus);

        return view('uniOrg::university.campus.show', compact([
            'campus'
        ]));
    }


}
