<?php

namespace Yeltrik\UniOrg\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Yeltrik\UniMbr\app\models\Dean;
use Yeltrik\UniOrg\app\models\College;
use Yeltrik\UniOrg\app\models\University;

class CollegeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param University $university
     * @return void
     * @throws AuthorizationException
     */
    public function index(University $university)
    {
        $this->authorize('viewAny', College::class);
        $colleges = $university->colleges;

        return view('uniOrg::university.college.index', compact([
            'university', 'colleges'
        ]));
    }

    /**
     * Display the specified resource.
     *
     * @param University $university
     * @param College $college
     * @return void
     * @throws AuthorizationException
     */
    public function show(University $university, College $college)
    {
        $this->authorize('view', $college);
        if ( class_exists(Dean::class)) {
            $deans = Dean::query()->where('college_id', '=', $college->id)->get();
        } else {
            $deans = new Collection();
        }

        $departments = $college->departments;

        return view('uniOrg::university.college.show', compact([
            'university', 'college', 'departments',
            'deans'
        ]));
    }

}
