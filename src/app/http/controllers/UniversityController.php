<?php

namespace Yeltrik\UniOrg\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Yeltrik\UniOrg\app\models\University;

class UniversityController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $universities = University::all();
        $this->authorize('viewAny', University::class);

        return view('uniOrg::university.index', compact('universities'));
    }

    /**
     * @param University $university
     * @throws AuthorizationException
     */
    public function show(University $university)
    {
        $this->authorize('view', $university);
        $campuses = $university->campuses;
        $colleges = $university->colleges;

        return view('uniOrg::university.show', compact(
            ['university', 'campuses', 'colleges']
        ));
    }

}
