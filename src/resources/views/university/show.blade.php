@extends('layouts.app')

@section('content')
    <div class="container">

        @include('uniOrg::university.campus.list-group')
        @include('uniOrg::university.college.list-group')

    </div>
@endsection
