@extends('layouts.app')

@section('content')
    <div class="container">

        @include('uniOrg::university.campus.list-group')

    </div>
@endsection
