@if($campuses->count() > 0)
    <div class="list-group">
        <a href="#" class="list-group-item list-group-item-action active">
            Campuses
        </a>
        @foreach($campuses as $campus)
            <a href="{{route('universities.campuses.show', [$university,$campus])}}"
               class="list-group-item list-group-item-action">{{$campus->name}}</a>
        @endforeach
    </div>
@endif
