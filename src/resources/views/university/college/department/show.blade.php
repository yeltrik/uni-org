@extends('layouts.app')

@section('content')
    <div class="container">

        <h3>
            Department: {{$department->name}}
        </h3>

        @includeIf('uniMbr::university.college.department.department-head.list-group')
        @includeIf('uniMbr::university.college.department.faculty.list-group')
        @includeIf('uniMbr::university.college.department.staff.list-group')

    </div>
@endsection
