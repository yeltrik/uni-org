@if($departments->count() > 0)
    <div class="list-group">
        <a href="#" class="list-group-item list-group-item-action active">
            Departments
        </a>
        @foreach($departments as $department)
            <a href="{{route('universities.departments.show', [$university, $department])}}"
               class="list-group-item list-group-item-action">{{$department->name}}</a>
        @endforeach
    </div>
@endif
