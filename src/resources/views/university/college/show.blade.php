@extends('layouts.app')

@section('content')
    <div class="container">

        <h3>
            College: {{$college->name}}
        </h3>

        @includeIf('uniMbr::university.college.dean.list-group')
        @include('uniOrg::university.college.department.list-group')

    </div>
@endsection
