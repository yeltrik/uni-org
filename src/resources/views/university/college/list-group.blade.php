@if($colleges->count() > 0)
    <div class="list-group">
        <a href="#" class="list-group-item list-group-item-action active">
            Colleges
        </a>
        @foreach($colleges as $college)
            <a href="{{route('universities.colleges.show', [$university, $college])}}"
               class="list-group-item list-group-item-action">{{$college->name}}</a>
        @endforeach
    </div>
@endif
