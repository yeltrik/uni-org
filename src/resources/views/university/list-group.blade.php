@if($universities->count() > 0)
    <div class="list-group">
        <a href="#" class="list-group-item list-group-item-action active">
            Universities
        </a>
        @foreach($universities as $university)
            <a href="{{route('universities.show', $university)}}"
               class="list-group-item list-group-item-action">{{$university->name}}</a>
        @endforeach
    </div>
@endif
