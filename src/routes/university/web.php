<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\UniOrg\app\http\controllers\UniversityController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/university',
    [UniversityController::class, 'index'])
    ->name('universities.index');

Route::get('/university/{university}',
    [UniversityController::class, 'show'])
    ->name('universities.show');

require 'campus/web.php';
require 'college/web.php';
require 'department/web.php';
