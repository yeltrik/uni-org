<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\UniOrg\app\http\controllers\DepartmentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/university/{university}/college/{college}/department',
    [DepartmentController::class, 'index'])
    ->name('universities.colleges.departments.index');


