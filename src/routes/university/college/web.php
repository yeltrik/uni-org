<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\UniOrg\app\http\controllers\CollegeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/university/{university}/college',
    [CollegeController::class, 'index'])
    ->name('universities.colleges.index');

Route::get('/university/{university}/college/{college}',
    [CollegeController::class, 'show'])
    ->name('universities.colleges.show');

require 'department/web.php';

