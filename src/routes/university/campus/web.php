<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\UniOrg\app\http\controllers\CampusController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/university/{university}/campus',
    [CampusController::class, 'index'])
    ->name('universities.campuses.index');

Route::get('/university/{university}/campus/{campus}',
    [CampusController::class, 'show'])
    ->name('universities.campuses.show');
