<?php

namespace Yeltrik\UniOrg\database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Yeltrik\UniOrg\app\models\Campus;

class CampusFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Campus::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'abbr' => $this->faker->randomLetter()
        ];
    }
}
