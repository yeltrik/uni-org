<?php

namespace Yeltrik\UniOrg\database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Yeltrik\UniOrg\app\models\Department;

class DepartmentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Department::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'abbr' => $this->faker->randomLetter()
        ];
    }
}
