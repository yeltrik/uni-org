<?php

namespace Yeltrik\UniOrg\database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Yeltrik\UniOrg\app\Models\University;

class UniversityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = University::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $state = $this->faker->state;
        $abbr = substr($state, 0,1) . 'U';
        return [
            'name' => "$state University",
            'abbr' => $abbr
        ];
    }
}
