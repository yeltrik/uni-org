<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('uni_org')->create('departments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('university_id')->nullable();
            $table->unsignedBigInteger('college_id')->nullable();
            $table->string('name', 255);
            $table->string('abbr', 128)->nullable();
            $table->timestamps();

            $table->unique(['university_id', 'college_id', 'name', 'abbr']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('uni_org')->dropIfExists('departments');
    }
}
