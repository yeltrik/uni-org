<?php

namespace Yeltrik\UniOrg\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\UniOrg\app\models\University;

class UniversitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $universities = University::factory()->count(2)->create();
    }
}
