<?php

namespace Yeltrik\UniOrg\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\UniOrg\app\models\College;
use Yeltrik\UniOrg\app\models\Department;
use Yeltrik\UniOrg\app\models\University;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $college = College::factory()
            ->for(University::factory())
            ->create();

        // Two Cases
        // Department for A University
        // Department for a University and College

        $departments = Department::factory()
            ->count(10)
            ->make();

        foreach ($departments as $department) {
            $department->university()->associate($college->university);
            $department->college()->associate($college);
            $department->save();
        }
    }
}
