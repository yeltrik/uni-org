<?php

namespace Yeltrik\UniOrg\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\UniOrg\app\models\Campus;
use Yeltrik\UniOrg\app\models\University;

class CampusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $campuses = Campus::factory()
            ->count(3)
            ->for(University::factory())
            ->create();
    }
}
