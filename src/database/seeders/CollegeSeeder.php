<?php

namespace Yeltrik\UniOrg\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\UniOrg\app\models\College;
use Yeltrik\UniOrg\app\models\University;

class CollegeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $colleges = College::factory()
            ->count(10)
            ->for(University::factory())
            ->create();
    }
}
