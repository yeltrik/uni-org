<?php

namespace Yeltrik\UniOrg\tests\feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Yeltrik\UniOrg\app\models\College;
use Yeltrik\UniOrg\app\models\Department;
use Yeltrik\UniOrg\app\models\University;

class DepartmentTest extends TestCase
{

    public function testRouteIndex()
    {
        $user = $this->getUser();
        $college = College::query()->inRandomOrder()->firstOrFail();
        $university = $college->university;
        $response = $this->actingAs($user, 'web')
            ->get(route('universities.colleges.departments.index', [$university, $college]));
        $response->assertStatus(200);
    }

    public function testRouteShow()
    {
        $user = $this->getUser();
        $department = Department::query()->inRandomOrder()->firstOrFail();
        $university = $department->university;
        $response = $this->actingAs($user, 'web')
            ->get(route('universities.departments.show', [$university, $department]));
        $response->assertStatus(200);
    }

    public function getUser()
    {
        return User::query()->inRandomOrder()->firstOrFail();
    }

}
