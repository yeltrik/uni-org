<?php

namespace Yeltrik\UniOrg\tests\feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Yeltrik\UniOrg\app\models\Campus;
use Yeltrik\UniOrg\app\models\University;

class CampusTest extends TestCase
{

    public function testRouteIndex()
    {
        $user = $this->getUser();
        $university = University::query()->inRandomOrder()->firstOrFail();
        $response = $this->actingAs($user, 'web')
            ->get(route('universities.campuses.index', [$university]));
        $response->assertStatus(200);
    }

    public function testRouteShow()
    {
        $user = $this->getUser();
        $campus = Campus::query()->inRandomOrder()->firstOrFail();
        $university = $campus->university;
        $response = $this->actingAs($user, 'web')
            ->get(route('universities.campuses.show', [$university, $campus]));
        $response->assertStatus(200);
    }

    public function getUser()
    {
        return User::query()->inRandomOrder()->firstOrFail();
    }

}
