<?php

namespace Yeltrik\UniOrg\tests\feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Yeltrik\UniOrg\app\models\College;
use Yeltrik\UniOrg\app\models\University;

class CollegeTest extends TestCase
{

    public function testRouteIndex()
    {
        $user = $this->getUser();
        $university = University::query()->inRandomOrder()->firstOrFail();
        $response = $this->actingAs($user, 'web')
            ->get(route('universities.colleges.index', [$university]));
        $response->assertStatus(200);
    }

    public function testRouteShow()
    {
        $user = $this->getUser();
        $college = College::query()->inRandomOrder()->firstOrFail();
        $university = $college->university;
        $response = $this->actingAs($user, 'web')
            ->get(route('universities.colleges.show', [$university, $college]));
        $response->assertStatus(200);
    }

    public function getUser()
    {
        return User::query()->inRandomOrder()->firstOrFail();
    }

}
